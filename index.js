'use strict';

const _ = require('lodash');
const got = require('got');

const create = config => {
    let opt = JSON.parse(_.isString(config) ? fs.readFileSync(file) : JSON.stringify(config));
    let uri = encodeURI(opt.entry);
    let send = _.partial(got.post.bind(got), uri);

    return {
        "sendToUser": (user, msg) => send({
            "form": true,
            "body": {
                "user": user,
                "message": msg
            }
        }),
        "sendToChannel": (channel, msg) => send({
            "form": true,
            "body": {
                "channel": channel,
                "message": msg
            }
        })
    }
}

module.exports = {
    "create": create
};
