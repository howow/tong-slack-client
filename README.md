# tong-slack-client

連 tong 送訊息

## Install

```
npm install bitbucket:howow/tong-slack-client
```

## Usage

注意: 要送訊息給 `channel` 必須將 bot `tong` 加入該 `channel` 。

```
let file = {"entry": "http://127.0.0.1/tong/slack"};  // file
const { sendToUser, sendToChannel } = require('tong-slack-client').create(file);

let user = 'user', message = 'test message';
sendToUser(user, message);

let channel='channel';
sendToChannel(channel, message);
```
